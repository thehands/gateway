# Gateway 2.0

## Prerequisites
Install alpine linux: 

1. Create bootable usb: https://wiki.alpinelinux.org/wiki/Create_a_Bootable_USB
2. Boot usb and follow wiki: https://wiki.alpinelinux.org/wiki/Installation

## Run Gateway
To run gateway:

1. Setup alpine: `setup-alpine`
2. `reboot`, **login as root** (you chose a password for root in setup-alpine)
3. Install a package to be able to connect to http: `apk add openssl`
4. Run: `wget -O- http://bitbucket.com/pollon/gateway/raw/master/gateway | sh`
5. Start gateway install: `./gateway`

## What it does

- Installs base packages for daily use (editor, browser, ect..)
- Adblock
- Xmonad & Haskell Stack
- Creates and configures a user
- Configures sudo

## What it should do

- Handle wifi somehow (wifish is dead, we need a new solution)
- Configure fonts

### Wifi Tips
Follow Alpine's wiki: 
  https://wiki.alpinelinux.org/wiki/Connecting_to_a_wireless_access_point

Install pkgs:`sudo apk add iw wpa_supplicant wireless-tools` 
(Note`iw` is not mentioned in the Alpine wifi page)

For additional troubleshooting check Arch wiki:
  https://wiki.archlinux.org/index.php/Wireless_network_configuration
