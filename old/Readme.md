# Gateway 
Gateway makes a lean linux install effortless.

## What?
Gateway is a linux install script (previously for Arch, currently for Void linux).

## Why?
Because the first step in converting a computer from a toy-box to a toolbox is 
having the freedom to erase your machine at any time (empty your toolbox); 
you can only have this confidence if reinstalling the basics (windowing 
environment, internet, terminal) is effortless.

## How?

### Prerequisites
- A fresh Void linux install
- A wired internet connection*

*Wireless is possible, but more complicated; help is here: 
[Network Configuration Void Wiki](https://wiki.voidlinux.eu/Network_Configuration)*

#### To get a fresh install: 

**Download void iso**

[Download the void .iso](https://repo.voidlinux.eu/live/current/). It will have a name like:  `void-live-x86_64-20160420.iso`.

**Prepare a bootable usb**

Write the iso to a usb: 

- Find usb device name (usually `sdb`): `$ lsblk`. *Note the `dd` command will erase all contents of the usb.*
- Write to usb: `sudo dd if=<filename.iso> of=/dev/<usbdevice> bs=4M; sync` *Note: fish shell syntax `;` vs. bash `&&`*

**Install Void**

Boot the void usb you just created; follow the instructions closely; use void's built in installer.

### Install Gateway

**Login**

Boot into your fresh void installation; login to your user account (non-root).

**Install git and fish-shell**

This also updates/syncs repositories correctly after a fresh install.

```
# xbps-install -Suy git fish-shell python
```


**Clone the gateway repository**

```
# git clone https://gitlab.com/thehands/gateway.git
```

**Start gateway**

This will start the gateway setup.

```
# cd gateway/
# ./gateway.fish
```

# Post gateway installation
Congrats! You have a fresh, functional linux install. But how to use it?

## The basics: navigation, applications, tools

## navigation
**The window manager**

When you first boot up and login to way os you're in virtual terminal. While VT
is pretty useful let's start somewhere more familiar: the window manager
(currently, a lightly configured [xmonad](http://xmonad.org/). 

- Start the window manager with: `xinit`.
- Spawn applications with dmenu by pressing the `MOD+Enter` (`MOD` is usually the windows or `Alt` key).
- Xmonad has [vi-bindings](https://github.com/martanne/vis), this means you can navigate windows with `MOD-j/k` and resize windows with `MOD-h/l`, change window layouts with `MOD-space`; close windows with `MOD-shift-c`; exit xmonad (back to virtual terminal) with `MOD-shift-q`.

**terminal**

[st](http://core.suckless.org/) is the default terminal (with [fish](https://fishshell.com/) as default shell); spawn with `MOD-enter`, then `st`.

**Internet**

- Start wifi with `wi`.
- Start the web browser by spawning a browser of your choice, e.g., `surf` (of course you'll need to install the surf browser `xi surf`)
- Test your connection with: `pn` (ping).


# Modules (for reference)

- xbps
- sudo
- fish
- ad-block
- multimedia
- xorg
- font
- utilities
- xmonad
- st
- vis-git
