#!/bin/sh

case "$1" in
    *.tar*) tar tf "$1";;
    *.gz*) tar tf "$1";;
    *.7z) 7z l "$1";;
    *) highlight -O ansi "$1" || cat "$1";;
esac