# Fish config file ~/.config/fish/config.fish

# Remove fish greeting.
set -gx fish_greeting ''

# Make man more usable.
set -gx MANPAGER less

# Set Path
if test -e ~/.cabal/bin
	set PATH $PATH ~/.cabal/bin
end
if test -e ~/.local/bin
 	set PATH $PATH ~/.local/bin
end
if test -e ~/.bin
 	set PATH $PATH ~/.bin
end
if test -e /opt/void-packages
  set PATH $PATH /opt/void-packages
end

# Linux
shortcut_method s sudo

# XBPS
shortcut_method xi "sudo xbps-install"
shortcut_method xq "sudo xbps-query"
shortcut_method xr "sudo xbps-remove"
shortcut_method xs "xbps-src"

# Wifi
shortcut_method wi "bash -c 'SCAN_SECONDS=5 wifish'"
shortcut_method wi "wpa_cli list_networks"
shortcut_method ws "wpa_cli status"
shortcut_method wsn "wpa_cli select_network"

# Utilities
shortcut_method ac "acpi"             # Get your power status.
shortcut_method pn "ping -c 3 ddg.gg" # Ping duckduckgo thrice.
shortcut_method zz "sudo zzz"         # Put the computer to sleep.
shortcut_method ab "abdco"            # Terminal session manager.
shortcut_method time "date"           # Get the current time.
