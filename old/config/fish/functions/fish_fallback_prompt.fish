function fish_fallback_prompt
	set_color $fish_color_cwd
	echo -n $USER
	set dir (basename $PWD)
	set_color normal
	if test $dir != $USER
		echo -n "."
		echo -n $dir
	end
	echo -n ') '
end
