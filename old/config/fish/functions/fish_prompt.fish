function fish_prompt
	set_color $fish_color_cwd
	echo -n $USER
	set dir (basename $PWD)
	if test $dir != $USER
		set_color white
		echo -n "."
		set_color yellow
		echo -n $dir
	end
	set_color white
	echo -n ') '
	set_color normal
end
