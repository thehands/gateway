import XMonad
import XMonad.Util.EZConfig
import XMonad.Layout.NoBorders

main = xmonad $ settings

-- Main configuration, override the defaults to your liking.
                
settings = defaultConfig {
  layoutHook = smartBorders $ layoutHook defaultConfig,
  -- ^Smart borders hides border when there is only one window
  borderWidth = 2,
  terminal = "st",
  normalBorderColor  = "#D7D7D7",
  focusedBorderColor = "#FFD75F",
  modMask = mod4Mask
  } `additionalKeysP` keyConfig

keyConfig = 
  [ ("M-<Return>", spawn "dmenu_run -p \">\" -fn \"Tamsyn-14\" -nb \"#212121\" -nf \"#9E9E9E\" -sb \"#424242\" -sf \"#FFD75F\"")
  ]
