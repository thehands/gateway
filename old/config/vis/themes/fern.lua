-- Fern Copyright (c) 2016 Logan Kaser
local lexers = vis.lexers

local colors = {
	['off0']    = '111',
	['off1']    = '188',
	['off2']    = '230',
	['grey0']   = '235',
	['grey1']   = '238',
	['grey2']   = '242',
	['grey3']   = '245',
  ['grey4']   = '249',
	['yellow']  = '222',
	['orange']  = '215',
	['red']     = '203',
	['magenta'] = '125',
	['violet']  = '61',
	['blue']    = '69',
	['cyan']    = '109',
	['green']   = '150',
}

lexers.colors = colors

-- dark
local fg = ',fore:'..colors.off1..','
local bg = ',back:'..colors.grey0..','

-- light
-- local fg = ',fore:'..colors.grey3..','
-- local bg = ',back:'..colors.off1..','

lexers.STYLE_DEFAULT = bg..fg
lexers.STYLE_NOTHING = bg
lexers.STYLE_CLASS = 'fore:'..colors.green
lexers.STYLE_COMMENT = 'fore:'..colors.grey2
lexers.STYLE_CONSTANT = 'fore:'..colors.red
lexers.STYLE_DEFINITION = 'fore:'..colors.red
lexers.STYLE_ERROR = 'fore:'..colors.red..',italics'
lexers.STYLE_FUNCTION = 'fore:'..colors.cyan
lexers.STYLE_KEYWORD = 'fore:'..colors.off0
lexers.STYLE_LABEL = 'fore:'..colors.grey3
lexers.STYLE_NUMBER = 'fore:'..colors.cyan
lexers.STYLE_OPERATOR = 'fore:'..colors.red
lexers.STYLE_REGEX = 'fore:'..colors.green
lexers.STYLE_STRING = 'fore:'..colors.off2
lexers.STYLE_PREPROCESSOR = 'fore:'..colors.orange
lexers.STYLE_TAG = 'fore:'..colors.red
lexers.STYLE_TYPE = 'fore:'..colors.yellow
lexers.STYLE_VARIABLE = 'fore:'..colors.grey4
lexers.STYLE_WHITESPACE = ''
lexers.STYLE_EMBEDDED = 'back:'..colors.blue
lexers.STYLE_IDENTIFIER = fg

lexers.STYLE_LINENUMBER = fg
lexers.STYLE_CURSOR = 'fore:'..colors.grey0..',back:'..colors.off1
lexers.STYLE_CURSOR_LINE = 'back:'..colors.grey1
lexers.STYLE_COLOR_COLUMN = 'back:'..colors.grey1
lexers.STYLE_SELECTION = 'back:'..colors.off1
