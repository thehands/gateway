#!/usr/bin/fish
# Ask user a yes or no question, then return 0 for yes and 1 for no as a $status.

function confirm
	while true
		read -p 'set_color green; echo -n "(yes"; set_color normal; echo -n "|"; set_color red; echo -n "no)"; set_color normal; echo -n ": "' -l choice
		switch $choice
			case Y y yes Yes ye Ye yup Yup affermative Affermative hai Hai
				return 0
				break
			case '' N n no No nah Nah nope Nope na Na negative Negative
				return 1
				break
		end
	end
end
