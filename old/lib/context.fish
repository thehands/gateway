#!/usr/bin/fish
# Helper function to give scripts context.

function context
	clear
	echo -n $argv[1];set_color -o yellow;echo -n " / ";set_color normal;echo $argv[2];set_color -o;hr
  for x in (seq $COLUMNS);
		echo -n -;
	end
	set_color normal
end
