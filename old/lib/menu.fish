#!/usr/bin/fish
# Displays a menu then returns the user's choice by echoing.

function menu
	while true
		read -p 'echo '';set_color yellow; echo "Enter a number."; set_color normal; echo -n $argv; echo -n " : "' -l choice
	echo $choice
	break
	end
end
