#!/usr/bin/fish
# Installs haskell tooling.

context "Gateway" "Haskell"
echo "Install Haskell tooling?"
if confirm
  sudo xbps-install -Sy cabal-install stack zlib-devel ncurses-devel
  stack install hlint
end
