#!/usr/bin/fish
# Configure fish shell.

context "Gateway" "Fish"
sudo chsh -s /usr/bin/fish $USER

cp ../config/fish/config.fish /tmp/config.fish
cp ../config/fish/functions/fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish
cp ../config/fish/functions/fish_fallback_prompt.fish ~/.config/fish/functions/fish_fallback_prompt.fish
set -U fish_greeting ''
mkdir -p ~/.local/bin
mkdir ~/.bin
mkdir -p ~/.cabal/bin

while true
	context "Gateway" "Fish"
	echo "Abbreviate or Alias?"
	echo "Some console commands are very long. To make them easier to type,"
	echo "Fish has two shortcut methods: `abbr` and `alias`."
	sleep 1
	echo "If unsure choose 1."
	switch (menu "1/abbreviate 2/alias")
		case 1
			echo ">> Applying `abbr` shortcut method to config.fish.."
			sed -i "s/\bshortcut_method\b/abbr/g" /tmp/config.fish
			sleep 1
			break
		case 2
			echo ">> Applying `alias` shortcut method to config.fish.."
			sed -i "s/\bshortcut_method\b/alias/g" /tmp/config.fish
			sleep 1
			break
	end
end

cp /tmp/config.fish ~/.config/fish/config.fish

echo "Install fish config for root as well?"
if confirm
	sudo chsh -s /usr/bin/fish root
	sudo mkdir -p /root/.config/fish
	sudo cp ~/.config/fish/config.fish /root/.config/fish/config.fish
	sudo mkdir -p root/.local/bin
	sudo mkdir root/.bin
	sudo mkdir -p root/.cabal/bin
	echo "Done."
	sleep 2
end
