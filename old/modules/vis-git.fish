#!/usr/bin/fish
# Install & Configure vis-git.

context "Gateway" "Vis"
echo "Install vis-git?"
if confirm
	sudo cp -r ../templates/vis-git /opt/void-packages/srcpkgs
	/opt/void-packages/xbps-src pkg vis-git
	sudo xbps-install --repository=/opt/void-packages/hostdir/binpkgs vis-git
	echo "Install Gateway vis config?"
	if confirm
		cp -r ../config/vis ~/.config
	end
end
