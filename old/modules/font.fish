#!/usr/bin/fish
# Configure fonts
# X11 fonts Require xorg or xwayland to be used.

function font_test
	echo "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ"
	echo "!@#\$%^&*()-_=+:;,.<>?|~`"
	echo "Sphinx of black quartz, judge my vow!"
	hr
end

function configfont
	sudo sed -i 's|#FONT=.*|FONT='$argv'|' /etc/rc.conf
end

sudo cp -pr ../font/tamsyn/*psf.gz /usr/share/kbd/consolefonts
while true
	context "Gateway" "Console Font"
	echo "Console fonts usually suck. Here are a couple that don't."
	echo "Pick a font to preview?"
	sleep 1
	switch (menu "1/Tamsyn Large  2/Tamsyn Small  3/Terminus Small")
		case 1
			setfont Tamsyn10x20r
			context "Gateway" "Console Font"
			echo "1/Tamsyn Large (20px):"
			font_test
			echo "Confirm choice? No returns to menu."
			if confirm
				configfont Tamsyn10x20r
				break
			end
		case 2
			setfont Tamsyn8x16r
			context "Gateway" "Console Font"
			echo "2/Tamsyn Small (16px):"
			font_test
			echo "Confirm choice? No returns to menu."
			if confirm
				configfont Tamsyn8x16r
				break
			end
		case 3
			setfont Lat2-Terminus16
			context "Gateway" "Console Font"
			echo "3/Terminus Small (16px):"
			font_test
			echo "Confirm choice? No returns to menu."
			if confirm
				configfont Lat2-Terminus16
				break
			end
	end
end

context "Gateway" "Font"

echo "Install fonts for X11?"
if confirm
	echo "Installing Tamsyn to /usr/share/fonts (For X11).."
	sudo mkdir -p /usr/share/fonts/Tamsyn
	sudo cp -pr ../font/tamsyn/*pcf.gz /usr/share/fonts/Tamsyn
	sudo chmod 0555 /usr/share/fonts/Tamsyn
	sudo chmod 0444 /usr/share/fonts/Tamsyn/*
	sudo fc-cache -fvs
	sleep 2
end
