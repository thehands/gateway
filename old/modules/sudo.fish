#!/usr/bin/fish
# Configures XBPS

context "Gateway" "Sudo"
echo -n "Should sudo require a password?"
echo "Requiring a password adds security, reduces errors."
echo ""
echo "If unsure choose password."
switch (menu "1/password 2/no password")
	case 1
		context "Gateway" "Sudo"
		echo "Modifying `/etc/sudoers` with require password settings.."
		sudo sed -i 's|# %wheel ALL=(ALL) ALL|%wheel ALL=(ALL) ALL|' /etc/sudoers
		sudo sed -i 's|%wheel ALL=(ALL) NOPASSWD: ALL|# %wheel ALL=(ALL) NOPASSWD: ALL|' /etc/sudoers
		sleep 1
	case 2
		context "Gateway" "Sudo"
		echo "Modifing `/etc/sudoers` with no password settings.."
		sudo sed -i 's|# %wheel ALL=(ALL) NOPASSWD: ALL|%wheel ALL=(ALL) NOPASSWD: ALL|' /etc/sudoers
		sudo sed -i 's|%wheel ALL=(ALL) ALL|# %wheel ALL=(ALL) ALL|' /etc/sudoers
		sleep 1
end

context "Gateway" "Sudo"
echo "Running visudo to check syntax of `/etc/sudoers` .."
sudo visudo -c -f /etc/sudoers
sleep 2
