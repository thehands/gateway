#!/usr/bin/fish
# Install & Configure Xmonad.
# Require Xorg.

context "Gateway" "Xmonad"
echo "Install XMonad?"
if confirm
	sudo xbps-install -y dmenu libX11-devel libXrandr-devel cabal-install
	cp ../config/xmonad/.xinitrc ~/.xinitrc
	mkdir ~/.xmonad
	cp ../config/xmonad/xmonad.hs ~/.xmonad/xmonad.hs
	cabal update
	cabal install xmonad xmonad-contrib --flags="-use_xft"
	~/.cabal/bin/xmonad --recompile
end
