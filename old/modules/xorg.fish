#!/usr/bin/fish
# Installs Xorg

context "Gateway" "Xorg"

echo "Install Xorg?"

if confirm
	sudo xbps-install -Sy xorg-minimal dejavu-fonts-ttf xinit xprop xset xrdb
end
