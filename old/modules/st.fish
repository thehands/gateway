#!/usr/bin/fish
# Install ST terminal emulator (suckless.org).
# Require Xorg or xWayland.

context "Gateway" "ST"
echo "Gateway uses ST as the X11 terminal emulator."
echo "Install opiniated ST? Otherwise standard ST will be installed."
if confirm
	sudo cp -r ../templates/st /opt/void-packages/srcpkgs
	/opt/void-packages/xbps-src pkg st
	sudo xbps-install -Syf --repository=/opt/void-packages/hostdir/binpkgs st
else
	sudo xbps-install -Sy st
end
