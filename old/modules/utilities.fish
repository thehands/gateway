#!/usr/bin/fish
# Configures utilities.

context "Gateway" "Utilities"
echo "Install recommended utilities?"
echo "Recommended utilities: wpa_supplicant wifish less dialog curl acpi"
if confirm
	sudo xbps-install -Sy wpa_supplicant wifish less dialog curl wget acpi
end

context "Gateway" "Utilities"
echo "Setup wifi?"
if confirm
	ip link
	echo "Let's find your WiFi device name. Example: \"wlp3s0\""
	read -p "echo Wifi card:" wifi_device
	if not test wifi_device = ""
		sudo cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant-$wifi_device.conf
		sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/libexec/dhcpcd-hooks
		sudo sv restart dhcpcd
		sleep 2
	else
		echo "Empty string is not a valid wifi device name, not setting up."
		sleep 2
	end
end

context "Gateway" "NTPD"
echo "Setup ntpd? (Network Time Sync)"
if confirm
  sudo xbps-install -Sy ntp
	context "Gateway" "NTPD"
	echo "Use gateway anti ntpd log spam hack?"
	echo "By default, ntpd prints some useless logs to console."
	echo "This hides that."
	if confirm
		sudo cp -p ../config/ntpd/run /etc/sv/ntpd/run
	end
	context "Gateway" "NTPD"
	sudo ln -s /etc/sv/ntpd /var/service/
	echo "Enableing ntpd."
	sleep 2
end
