#!/usr/bin/fish
# Installs enviroment and utilities 
# for multimedia and graphical applications.

context "Gateway" "Graphics"
echo "Choose graphics vender."
echo "If unsure choose 1."
switch (menu "1/intel 2/nvdia 3/amd")
	case 1 intel
		sudo xbps-install -y xf86-video-intel
	case 2 nvdia
		sudo xbps-install -y xf86-video-nouveau
	case 3 amd
		sudo xbps-install -y xf86-video-ati
end

context "Gateway" "Audio"
echo "Install Pulseaudio?"
if confirm
	sudo xbps-install -Sy pulseaudio ConsoleKit2 pavucontrol
  sudo ln -s /etc/sv/dbus /var/service/
  sudo ln -s /etc/sv/cgmanager /var/service
  sudo ln -s /etc/sv/consolekit /var/service/
end
