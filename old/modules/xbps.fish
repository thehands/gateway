#!/usr/bin/fish
# Configures XBPS

context "Gateway" "XBPS"
set dir (pwd)
cd /tmp
echo "repository=https://repo.voidlinux.eu/current/nonfree" > ./gateway.conf
echo "repository=/opt/void-packages/hostdir/binpkgs" >> ./gateway.conf
sudo mv ./gateway.conf /etc/xbps.d/
echo "/etc/xbps.d/gateway.conf"\n
cat /etc/xbps.d/gateway.conf
echo
hr
echo "Enabled non-free and local repositories.."
sleep 2
git clone git://github.com/voidlinux/void-packages.git
context "Gateway" "XBPS"
sudo mv ./void-packages /opt/
set -gx PATH $PATH /opt/void-packages
cd /opt/void-packages
xbps-src binary-bootstrap
cd $dir
context "Gateway" "XBPS"
echo "Installed xbps-src."
sleep 2
