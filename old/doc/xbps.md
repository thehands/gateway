## XBPS  | X Binary Package System
---
### xq (xbps-query)

| Command | Effect
| --- | --- |
`xq <package name>` | Get package information
`xq -Rs <package name>` | Search for a package
`xq -m` | List user installed packages
`xq -l` | List all installed packages
`xq -o` | List all orphan packages

### xi (xbps-install)

| Command | Effect
| --- | --- |
`xi <package name>` | Install package
`xi -S` | Sync package lists first
`xi -u` | Upgrade package
`xi -y` | No confirm

### xr (xbps-remove)

| Command | Effect
| --- | --- |
`xr <package name>` | Remove package
`xr -R` | Recursively remove dependencies
`xr -o` | Remove all orphan packages
`xr -O` | Clean package cache